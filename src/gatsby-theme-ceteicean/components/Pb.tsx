import React from "react"
import { Behavior } from "gatsby-theme-ceteicean/src/components/Behavior"
import { makeStyles } from '@material-ui/core/styles'
import Accordion from '@material-ui/core/Accordion'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import theme from "../../theme"
import { Fac } from "./Ceteicean"

import Loadable from "@loadable/component"
const IIIFViewer = Loadable(() => import("../../components/IIIFViewer"))

interface TEIProps {
  teiNode: Node,
  availableRoutes?: string[]
  facs: Fac[]
}

export type PbBehavior = (props: TEIProps) => JSX.Element | null

const Pb: PbBehavior = ({teiNode, facs}: TEIProps) => {
  const useStyles = makeStyles(() => ({
    root: {
      width: '100%',
      margin: '1em 0',
      transition: 'all .5s ease-in-out',
      gridArea: "1/1",
      position: "relative",
      zIndex: 1,
      backgroundColor: 'transparent',
    },
    expanded: {
      transition: 'all .5s ease-in-out',
      width: '100%',
      margin: '1em 0'
    },
    ease: {
      transition: 'all .5s ease-in-out',
    },
    container: {
      display: 'grid',
    },
    background: {
      width: '100%',
      margin: '1em 0',
      zIndex: -1,
      gridArea: "1/1",
      height: '55px',
      opacity: .2,
      overflow: 'clip',
      objectFit: 'cover',
    },
    simple: {
      padding: '1em'
    },
    summary: {
      margin: '8px 0 0 0',
    },
    heading: {
      fontSize: theme.typography.pxToRem(17),
      flexBasis: '33.33%',
      flexShrink: 0,
    },
    secondaryHeading: {      
      fontSize: theme.typography.pxToRem(17),
      color: theme.palette.text.secondary,      
    },
    secondaryHeadingMd: {
      flexBasis: '33.33%',
      flexShrink: 0,
      textAlign: 'center',
    },
    viewer: {
      justifyContent: "center",
    },
    fallback: {
      maxWidth: "100%"
    }
  }))
  const classes = useStyles()

  const [iiifFail, setIiifFail] = React.useState(false)
  
  const pb = teiNode as Element
  const n = pb.getAttribute('n') || ''
  
  // Show the first page expanded by default.
  const [expanded, setExpanded] = React.useState(false)
  const handleChange = () => {
    setExpanded(!expanded)
  }

  const facRef = pb.getAttribute('facs') || ''
  const img = facs.filter(f => f.label === facRef)[0]

  let page: JSX.Element | undefined = undefined

  React.useEffect(() => {
    async function checkImg() {
      if (img) {
        try {
          const res = await fetch(img.preview, { method: 'HEAD' })
          if (!res.ok) {
            setIiifFail(true)
          }
        } catch (err) {
          if (err) {
            setIiifFail(true)
          }
        }
      }
    }
    checkImg()
  }, [])

  if (img) {

    const fallback = `fallback/${img.preview.split('/')[5].split('%2')[3]}.jpg`
    const preview = iiifFail ? fallback : img.preview
    const image = iiifFail 
      ? <img src={fallback} alt="" className={classes.fallback} />
      : <IIIFViewer width={"100%"} height="600px" key={img.label} iiifUrl={`${img.url}/info.json`} />

    const background: JSX.Element | undefined = expanded ? undefined : (<img
      className={`${classes.background} ${classes.ease}`}
      src={preview}
      alt=""/>)
    page = (
      <div className={classes.container}>
        {background}
        <Accordion expanded={expanded} classes={{root: classes.root, expanded: classes.expanded}} onChange={() => handleChange()}>
          <AccordionSummary    
            classes={{content: classes.summary}}
            expandIcon={<ExpandMoreIcon />}
            aria-controls="pb-content"
            id="pb-header"
          >
            <Typography className={classes.heading} variant="body2">{n}</Typography>
          </AccordionSummary>
          <AccordionDetails className={classes.viewer}>
            {image}
          </AccordionDetails>
        </Accordion>
      </div>
    )
  } else if (n) {
    page = <Paper className={`${classes.root} ${classes.simple}`} elevation={1}>{n}</Paper>
  }

  if (n) {
    return (
      <Behavior node={teiNode}>        
        {page}
      </Behavior>    
    )
  }
  return null
}

export default Pb
