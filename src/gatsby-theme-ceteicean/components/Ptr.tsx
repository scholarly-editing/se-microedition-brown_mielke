import React from "react"

import { TBehavior, SafeUnchangedNode } from "gatsby-theme-ceteicean/src/components/DefaultBehaviors"

import { Behavior } from "gatsby-theme-ceteicean/src/components/Behavior"

import { makeStyles, withStyles } from "@material-ui/core/styles"
import Badge from "@material-ui/core/Badge"
import { NoteContext, TNote } from "./Ceteicean"

import { Colors, IColors } from '../../displayOptions'


type TEIProps = {
  teiNode: Node,
  availableRoutes?: string[]
}

const Footnote = withStyles((theme) => ({
  badge: {
    top: '-.5em',
    right: (props: {length: number, typeColor: string}) => {
      switch (props.length) {
        case 1:
          return `.75rem`
        case 3:
          return `.75rem`
        default:
          return `${props.length/2}rem`
      }
    },
    border: `2px solid ${theme.palette.background.paper}`,
    padding: '0 4px',
    cursor: 'pointer',
    textIndent: 0,
  },
  colorPrimary: {
    backgroundColor: (props: {length: number, typeColor: string}) => props.typeColor,
  }
}))(Badge)

const Ptr: TBehavior = (props: TEIProps) => {
  const useStyles = makeStyles(() => ({
    spacer: {
      width: (props: {length: number}) => {
        switch (props.length) {
          case 1:
            return `1.5rem`
          case 3:
            return `2rem`
          default:
            return `${props.length}rem`
        }
      }
    }
  }))

  const { setNote } = React.useContext(NoteContext)
  
  const el = props.teiNode as Element
  const n = el.getAttribute('n')
  const target = el.getAttribute('target')
  const id = target?.replace('#', '') || ''
  const note = el.ownerDocument.getElementById(id)

  const type = note?.getAttribute("type") || ""
  const typeColor = Colors[type as keyof IColors]

  if (n && target) {
    const noteData: TNote = {
      id,
      n: parseInt(n)
    } 
    const classes = useStyles({length: n.length})
    return (
      <Behavior node={props.teiNode} key={n}>
        <Footnote length={n.length} badgeContent={n} color="primary" max={999} typeColor={typeColor} onClick={() => {
          setNote(noteData)
        }}>
          <div className={classes.spacer}/>
        </Footnote>
      </Behavior>
    )
  }
  return <SafeUnchangedNode {...props} />
}

export default Ptr
