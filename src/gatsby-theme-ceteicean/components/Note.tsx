import React from "react"

import { Behavior } from "gatsby-theme-ceteicean/src/components/Behavior"
import { TEINodes } from "react-teirouter"

import { makeStyles } from "@material-ui/core/styles"

import { NoteContext } from "./Ceteicean"
import Card from "@material-ui/core/Card"
import CardHeader from "@material-ui/core/CardHeader"
import CardContent from "@material-ui/core/CardContent"
import useMediaQuery from '@material-ui/core/useMediaQuery'

import theme from '../../theme'
import IconButton from "@material-ui/core/IconButton"
import { Close } from "@material-ui/icons"
import Dialog from "@material-ui/core/Dialog"
import { TransitionProps } from "@material-ui/core/transitions/transition"
import Slide from "@material-ui/core/Slide"
import DialogTitle from "@material-ui/core/DialogTitle"
import Typography from "@material-ui/core/Typography"
import { Chip, DialogContent, DialogContentText } from "@material-ui/core"

import { Colors, IColors } from '../../displayOptions'

type TEIProps = {
  teiNode: Node,
  availableRoutes?: string[]
  isSynoptic: boolean
}

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement<any, any> },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />
})

export type NoteBehavior = (props: TEIProps) => JSX.Element | null

const Note: NoteBehavior = (props: TEIProps) => {
  const useStyles = makeStyles(() => ({
    card: {
      maxWidth: "300px",
      position: "absolute",
      right: () => props.isSynoptic ? "20px" : "50px",
    },
    dialogTitle: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center"
    }
  }))

  const { note, setNote } = React.useContext(NoteContext)
  const [cardPosition, setCardPosition] = React.useState(350)

  const isScreenSmall = useMediaQuery(theme.breakpoints.down('lg'))

  React.useEffect(() => {
    const fromTop = document.documentElement.scrollTop > 150 ? document.documentElement.scrollTop + 100 : document.body.scrollTop
    setCardPosition(fromTop > 0 ? fromTop : 350)
  }, [note])
  
  const el = props.teiNode as Element
  const noteId = el.getAttribute('id')
  const type = el.getAttribute('type')
  const typeColor = Colors[type as keyof IColors]

  const classes = useStyles()
  if (note) {
    if (note.id === noteId) {
  
      let content: JSX.Element | undefined = undefined
      const closeNote = (<IconButton aria-label="close person info" onClick={() => setNote(null)}>
        <Close />
      </IconButton>)

      if (isScreenSmall) {
        content = (
          <Dialog
            open={note.hasOwnProperty('id')}
            scroll="body"
            TransitionComponent={Transition}
            keepMounted
            onClose={() => setNote(null)}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
          >
            <DialogTitle id="alert-dialog-slide-title" disableTypography className={classes.dialogTitle}>
              <Typography variant="h6">Note {note.n}<br/><Chip size="small" label={type} style={{
                  backgroundColor: typeColor,
                  color: "#fff"}} /></Typography>
              <IconButton aria-label="close person info" onClick={() => setNote(null)}>
                <Close />
              </IconButton>
            </DialogTitle>
            <DialogContent>
              <DialogContentText component="div" id="alert-dialog-slide-description">
                <TEINodes 
                  teiNodes={props.teiNode.childNodes}
                  {...props}/>
              </DialogContentText>
            </DialogContent>
          </Dialog>
        )
      } else {
        content = (
          <Card style={{top: cardPosition}} className={classes.card}>
            <CardHeader
              action={closeNote}
              title={<>Note {note.n}<br/><Chip size="small" label={type}  style={{
                backgroundColor: typeColor,
                color: "#fff"
              }} /></>}
            />
            <CardContent>
              <TEINodes 
                teiNodes={props.teiNode.childNodes}
                {...props}/>
            </CardContent>
          </Card>
        )
      }
  
      return (
        <Behavior node={props.teiNode}>
          {content}
        </Behavior>
      )
    }

  }
  return null
}

export default Note
