import React from "react"
import Ceteicean, {Routes} from "gatsby-theme-ceteicean/src/components/Ceteicean"
import {
  TeiHeader,
  Ref
} from "gatsby-theme-ceteicean/src/components/DefaultBehaviors"

import { ThemeProvider } from "@material-ui/core/styles"
import CssBaseline from "@material-ui/core/CssBaseline"
import Container, {ContainerProps} from "@material-ui/core/Container"

import theme from "../../theme"
import useDisplayStyles from "../../displayOptions"

import Layout from "../../components/layout"
import MicroEdAppbar from "../../components/microedAppbar"
import Ptr from "./Ptr"
import Note from "./Note"
import { graphql, useStaticQuery } from "gatsby"
import Pb from "./Pb"

import SEO from "../../components/seo"

interface Props {
  pageContext: {
    name: string
    prefixed: string
    elements: string[]
  },
  location: any
}

export interface IOptions { [key: string]: boolean }

export type ContextType = {
  contextOpts: IOptions
  setContextOpts: React.Dispatch<React.SetStateAction<IOptions>>
}

export const DisplayContext = React.createContext<ContextType>({
  contextOpts: {},
  setContextOpts: () => console.warn('no DisplayContext options provider')
})

export type Fac = {
  label: string
  preview: string
  url: string
}

export type TNote = {
  id: string
  n: number
}

type NoteContextType = {
  note: TNote | null
  setNote: React.Dispatch<React.SetStateAction<TNote | null>>
}

export const NoteContext = React.createContext<NoteContextType>({
  note: null,
  setNote: () => console.warn('no note data provider')
})

export default function MicroEdCeteicean({pageContext}: Props) {
  const displayStyles: { [key: string]: string } = useDisplayStyles()
  let display = ""
  let width: ContainerProps["maxWidth"] = "md"
  let isSynoptic = false

  if (pageContext.name.includes("synoptic")) {
    isSynoptic = true
    display = displayStyles.Synoptic
    width = "lg"
  }

  const startOpts: IOptions = pageContext.name.startsWith("synoptic") ? {HideDeletions: true} : {HighlightAdditions: true}

  const [displayOpts, setDisplayOpts] = React.useState(startOpts)
  const [note, setNote] = React.useState<TNote | null>(null)

  for (const k in displayOpts) {
    const key = k as keyof IOptions
    if (displayOpts[key]) {
      const normKey = key.toString().replace(/[_\s']+/g, '')
      display += ` ${displayStyles[normKey]}`
    }
  }

  const queryData = useStaticQuery(graphql`
    query iiifData {
      allDataJson {
        nodes {
          sequences {
            canvases {
              label
              images {
                resource {
                  _id
                  service {
                    _id
                  }
                }
              }
            }
          }
        }
      }
    } 
  `)
  const facs: Fac[] = queryData.allDataJson.nodes[0].sequences[0].canvases.map((c: any) => {
    return {
      url: c.images[0].resource.service._id,
      preview: c.images[0].resource._id,
      label: c.label
    }
  })

  const routes: Routes = {
    "tei-teiheader": TeiHeader,
    "tei-ptr": Ptr,
    "tei-ref": Ref,
    'tei-note': (props) => <Note isSynoptic={isSynoptic} {...props}/>,
  }

  if (!isSynoptic) {
    routes['tei-pb'] = (props) => <Pb facs={facs} {...props}/>
  }

  return(
    <DisplayContext.Provider value={{
      contextOpts: displayOpts,
      setContextOpts: setDisplayOpts
    }}>
      <NoteContext.Provider value={{note, setNote}}>
        <Layout appbar={<MicroEdAppbar location={pageContext.name}/>} location={pageContext.name.replace(/\/+$/, "")}>
          <SEO title="Edition" />          
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <Container maxWidth={width} component="main" className={display}>
              <Ceteicean pageContext={pageContext} routes={routes} />
            </Container>
          </ThemeProvider>
        </Layout>
      </NoteContext.Provider>
    </DisplayContext.Provider>
  )

}
