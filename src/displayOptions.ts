import { makeStyles } from '@material-ui/core/styles'

export interface IColors { [key: string]: string }

export const Colors: IColors = {
  HighlightAdditions: "#add8e6",
  definition: "rgb(68, 187, 153)",
  historical: "rgb(238, 136, 102)",
  musical: "rgb(238, 221, 136)",
  theatrical: "rgb(170, 68, 153)",
}

const useDisplayStyles = makeStyles(() => ({
  Synoptic: {
    "& > tei-tei": {
      display: "flex",
      flexDirection: "row",
      width: "100%",
    },
    "& tei-front, & tei-div[type=act], & tei-div[type=scene], & #bca2s2": {
      display: "flex",
      flexBasis: "100%",
      flexDirection: "column",
      flex: "1",
      padding: "0 1em"
    }
  },
  HighlightAdditions: {
    "& tei-add, .se_add": {
     borderBottom: `4px solid ${Colors.HighlightAdditions}`
    },
    "& tei-add > tei-stage": {
      borderBottom: `4px solid ${Colors.HighlightAdditions}`
    }
  },
  HideDeletions: {
    "& tei-del, .se_del": {
      display: "none"
    },
  }
}))

export default useDisplayStyles