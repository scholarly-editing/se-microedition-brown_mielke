import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import CssBaseline from "@material-ui/core/CssBaseline"
import { ThemeProvider, makeStyles } from "@material-ui/core/styles"

import theme from "../theme"
import Header from "./header"
import Footer from "./footer"
import EditionFooter from "./editionFooter"

import Backdrop from "@material-ui/core/Backdrop"
import { Rotate90DegreesCcw } from "@material-ui/icons"
import { Typography, useMediaQuery } from "@material-ui/core"

// Style

const useStyles = makeStyles((theme) => ({
  main: {
    paddingBottom: "1.45rem",
    "& h2, & h3": {
      paddingBottom: "1rem",
    },
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
  },
  backdropText: {
    backgroundColor: '#fff',
    padding: '2em'
  },
  backdropIcon: {
    display: 'block',
    fontSize: "150%"
  }
}))

// Component

interface Props {
  location?: string
  appbar?: JSX.Element
  children?: JSX.Element | JSX.Element[]
}

const Layout = ({ location, appbar, children }: Props) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          doi
          title
          menuLinks {
            name
            link
          }
        }
      }
    }
  `)

  const classes = useStyles()
  const [open, setOpen] = React.useState(true)

  let backdrop: JSX.Element | undefined
  
  if (location?.startsWith('synoptic') && useMediaQuery('(max-width:500px)')) {
    backdrop = <Backdrop className={classes.backdrop} open={open} onClick={() => setOpen(false)}>
      <Typography component="mark" variant="h4" className={classes.backdropText}>
        <Rotate90DegreesCcw className={classes.backdropIcon} />
        Rotate your device to a landscape (horizontal) position for the Synoptic view 
        <br/><br/>
        <em>Tap to dismiss</em>
      </Typography>
    </Backdrop>
  }

  const footer = location !== "intro" ? <EditionFooter/> : <Footer/>

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header
        location={location || ''}
        siteTitle={data.site.siteMetadata.title}
        menuLinks={data.site.siteMetadata.menuLinks}
        doi={data.site.siteMetadata.doi}
      />
      {appbar}
      <div className={classes.main}>
        {backdrop}
        {children}
      </div>
      {footer}
    </ThemeProvider>
  )
}

export default Layout
