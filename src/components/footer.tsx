import React from "react"
import Grid from "@material-ui/core/Grid"
import Container from "@material-ui/core/Container"
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles({
  footer: {
    backgroundColor: "#efefef",
    padding: "1rem 0",
    borderTop: "1px solid #dadada",
    fontSize: "1rem"
  },
  logo: {
    textAlign: "right",
  }
})

const Footer = () => {
    const classes = useStyles()

  return (<footer className={classes.footer}>
    <Container maxWidth="lg">
      <Grid container={true}>
        <Grid item={true} xs={9}>
          <a
            rel="license"
            href="http://creativecommons.org/licenses/by-nc-sa/3.0/"
          >
            <img
              alt="Creative Commons License"
              src="http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png"
            />
          </a>
          <br />
          This work is licensed under a{" "}
          <a
            rel="license"
            href="https://creativecommons.org/licenses/by-nc-sa/4.0/"
          >
            Creative Commons Attribution-NonCommercial-ShareAlike 4.0
            International License
          </a>
          . 
          <br /> The open source code for this micro-edition is available at <a href="https://gitlab.com/scholarly-editing/se-microedition-brown_mielke/">
          https://gitlab.com/scholarly-editing/se-microedition-brown_mielke/</a>.
          <br /> © {new Date().getFullYear()} Scholarly Editing.
          <br /> ISSN 2167-1257 | DOI <a href="https://doi.org/10.55520/6ZH06EW2">10.55520/6ZH06EW2</a>
        </Grid>
        <Grid item={true} xs={3} className={classes.logo}>
          <a href="http://www.documentaryediting.org">
            <img
              src="https://scholarlyediting.reclaim.hosting/se-archive/template_images/adelogo.png"
              alt="Logo of the Association for Documentary Editing"
            />
          </a>
        </Grid>
      </Grid>
    </Container>
  </footer>)
}

export default Footer