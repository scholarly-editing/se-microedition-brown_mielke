import React from "react"

import Container from "@material-ui/core/Container"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import useScrollTrigger from '@material-ui/core/useScrollTrigger'

import { makeStyles } from '@material-ui/core/styles'
import Button from "@material-ui/core/Button"
import { navigate } from "gatsby-link"
import DisplayOptionsMenu from "./displayOptionsMenu"
import DisplayOptions from "./displayOptions"
import { DisplayContext } from "../gatsby-theme-ceteicean/components/Ceteicean"
import { MenuItem, Typography, useMediaQuery } from "@material-ui/core"
import theme from "../theme"

const useStyles = makeStyles(() => ({
  appbarContent: {
    display: 'flex'
  },
  appbarTop: {
    marginTop: '-1.5rem',
    marginBottom: '2.5em'
  },
  appbarSpacer: {
    flex: '1 1 auto'
  },
  menuLabel: {
    padding: '6px 16px'
  }
}))

function FixedScroll({children}: {children: JSX.Element}) {
  const classes = useStyles()
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 250,
  })

  return React.cloneElement(children, {
    position: trigger ? 'fixed' : 'static',
    className: trigger ? '' : classes.appbarTop
  })
}

type Props = {
  location: string
}

export default function MicroedAppBar({location}: Props) {
  const classes = useStyles()
  const isScreenSmall = useMediaQuery(theme.breakpoints.down('md'))
  const isSynoptic = location.includes('synoptic')

  const getLocationLabel = () => {
    if (location.endsWith('front')) {
      return 'Cast'
    } else if (location.endsWith('1-2')) {
      return 'Act 2: Scenes 1 and 2'
    } else {
      const scene = isSynoptic ? location.substr(location.indexOf('-') + 1) : location.substr(location.indexOf('_') + 1)
      return scene.replace('_', ' ').replace('t1', 't 1').replace(/e(\d)/, 'e $1')
    }
  }

  const handleSceneClick = (here: boolean, dest: string) => {
    if (here) {
      return null
    }
    const prefix = isSynoptic ? 'synoptic-' : location.substr(0, location.indexOf('_')) + '_'
    navigate(`/${prefix}${dest}`)
  }

  const makeNavButton = (dest: string, label: string) => {
    const here = location.endsWith(dest)
    return <Button color="secondary" 
      variant={here ? "outlined" : "text"}
      onClick={() => handleSceneClick(here, dest)}>
      {label}
    </Button>
  }

  const makeMenuButton = (dest: string, label: string) => {
    const here = location.endsWith(dest)
    return <MenuItem onClick={() => handleSceneClick(here, dest)}>
      {label}
    </MenuItem>
  }

  const Act2Nav = location.startsWith('BC') 
    ? makeNavButton('Act2_Scene1', 'Scene 1')
    : makeNavButton('Act2_Scenes1-2', 'Scenes 1 and 2')

  const Act2Btn = location.startsWith('BC') 
    ? makeMenuButton('Act2_Scene1', 'Act 2: Scene 1')
    : makeMenuButton('Act2_Scenes1-2', 'Act 2: Scenes 1 and 2')

  const displayOpts = React.useContext(DisplayContext)

  const options = location.startsWith('BC') ? '' : (<>
    <div className={classes.appbarSpacer}/>
    <DisplayOptionsMenu label="Display Options">
      <DisplayOptions
        context={displayOpts}
        options={['Highlight Additions', 'Hide Deletions']} />
    </DisplayOptionsMenu>
  </>)

  const sections = isScreenSmall ? <DisplayOptionsMenu label={getLocationLabel()} forceLabel>
    <>
      {makeMenuButton('front', 'Cast')}
      {makeMenuButton('Act1_Scene1', 'Act 1: Scene 1')}
      {makeMenuButton('Act1_Scene2', 'Act 1: Scene 2')}
      {makeMenuButton('Act1_Scene3', 'Act 1: Scene 3')}
      {Act2Btn}
    </>
  </DisplayOptionsMenu>
  : <>{makeNavButton('front', 'cast')}
    <Typography variant="button" className={classes.menuLabel}>Act 1:</Typography>
    {makeNavButton('Act1_Scene1', 'Scene 1')}
    {makeNavButton('Act1_Scene2', 'Scene 2')}
    {makeNavButton('Act1_Scene3', 'Scene 3')}
    <Typography variant="button" className={classes.menuLabel}>Act 2:</Typography>
    {Act2Nav}</>

  return (<FixedScroll>
    <AppBar>
      <Toolbar>
        <Container maxWidth="md" className={classes.appbarContent}>
          {sections}         
          {options}
        </Container>
      </Toolbar>
    </AppBar>
  </FixedScroll>)
}