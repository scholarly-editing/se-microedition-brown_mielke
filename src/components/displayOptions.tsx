import React from "react"

import MenuItem from '@material-ui/core/MenuItem'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'

import { withStyles } from '@material-ui/core/styles'

import { Colors, IColors } from '../displayOptions'
import { ContextType, IOptions } from '../gatsby-theme-ceteicean/components/Ceteicean'

import theme from '../theme'

const OptionSwitch = withStyles({
  switchBase: (props: {customColor: string}) => ({
    '&.Mui-checked': {
      color: props.customColor,
    },
    "&.Mui-checked + .MuiSwitch-track": {
      backgroundColor: props.customColor,
    }
  }),
})(Switch)

interface Props {
  context: ContextType
  options: string[]
  customColors?: boolean
  all?: boolean
}

export default function DisplayOptions({context, options, customColors = false, all = false}: Props) {  

  const { contextOpts, setContextOpts } = context

  // All switch  
  const [allChecked, setAllChecked] = React.useState(Object.keys(contextOpts).length === options.length)

  const toggleOpt = (key: keyof IOptions) => {
    const newOpts: IOptions = Object.assign({}, contextOpts)
    if (key in contextOpts) {
      delete newOpts[key]
      setAllChecked(false)
    } else {
      newOpts[key] = true
    }
    setContextOpts(newOpts)
  }

  const toggleAllOpts = () => {
    const newOpts: IOptions = {}
    if (!allChecked) {
      for (const k of options) {
        const key = k.replace(/[_-\s']+/g, '')
        newOpts[key] = true
      }
    }
    setContextOpts(newOpts)
    setAllChecked(!allChecked)
  }

  let allSwitch: JSX.Element | null = null

  if (all) {
    allSwitch = (<MenuItem
      onKeyUp={(e: React.KeyboardEvent) => {
        if (e.key === " ") {
          toggleAllOpts()
        }
      }}>
      <FormControlLabel
        control={
          <OptionSwitch
            customColor={theme.palette.primary.main}
            name="All"
            checked={allChecked}
            onClick={() => toggleAllOpts()}
          />
        }
        label="All" 
      />
    </MenuItem>)
  }

  /* FIXME: when updating to M-UI 5.0 we should be able to use ref normally */
  return (<>
    {allSwitch}
    {options.map((item: string, idx) => {
      let color = theme.palette.primary.main
      const key = item.replace(/[_-\s']+/g, '') as keyof IOptions
      if (customColors) {
        color = Colors[key as keyof IColors]
      }
      return (<MenuItem key={idx}
        onKeyUp={(e: React.KeyboardEvent) => {
          if (e.key === " ") {
            toggleOpt(key)
          }
        }}>
        <FormControlLabel
          control={
            <OptionSwitch
              customColor={color}
              name={item}
              checked={contextOpts[key] || false}
              onClick={() => toggleOpt(key)}
            />
          }
          label={item} 
        />
      </MenuItem>
    )})}
  </>)

}