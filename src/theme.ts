import { red } from "@material-ui/core/colors"
import { createMuiTheme } from "@material-ui/core/styles"

const mainColor = "#dc3522"

// A custom theme for CETEIcean
// It is not intended to be comprehensive. The rules here are largely derived from earlier work on TEI Boilerplate  
const theme = createMuiTheme({
  typography: {
    fontFamily: "EB Garamond, Serif",
    body1: {
      fontSize: "1.25rem",
      paddingBottom: "1.25rem",
    },
    body2: {
      fontSize: "1rem"
    },
    subtitle1: {
      fontSize: "1.4rem",
    }
  },
  overrides: {
    MuiCssBaseline: {
      "@global": {
        "@font-face": [
          {
            fontFamily: "EB Garamond",
            fontStyle: "normal",
            fontDisplay: "swap",
            fontWeight: 400,
          },
        ],
        "a, a:visited, a:hover, a:active": {
          color: mainColor,
        },
        body: {
          color: "#444",
          fontSize: "1.25rem"
        },
        "h1, h2, h3, h4, h5, h6": {
          color: "#333",
        },
        "tei-choice tei-abbr + tei-expan:before, tei-choice tei-expan + tei-abbr:before, tei-choice tei-sic + tei-corr:before, tei-choice tei-corr + tei-sic:before, tei-choice tei-orig + tei-reg:before, tei-choice tei-reg + tei-orig:before": {
          content: `" ("`
        },
        "tei-choice tei-abbr + tei-expan:after, tei-choice tei-expan + tei-abbr:after, tei-choice tei-sic + tei-corr:after, tei-choice tei-corr + tei-sic:after, tei-choice tei-orig + tei-reg:after, tei-choice tei-reg + tei-orig:after": {
          content: `")"`
        },
        "tei-ab": {
          display: "block",
          marginTop: "1em",
          marginBottom: "1em",
        },
        "tei-emph": {
          fontStyle: "italic"
        }, 
        "tei-speaker": {
          fontStyle: "italic"
        }, 
        "tei-name": {
          fontStyle: "italic"
        }, 
        "tei-title": {
          fontStyle: "italic"
        }, 
        "tei-foreign": {
          fontStyle: "italic"
        }, 
        "tei-hi[rend=italics]": {
          fontStyle: "italic"
        },
        "tei-l[rend=center]": {
          textAlign: "center"
        },
        "tei-head[rend=center]": {
          textAlign: "center"
        },
        "tei-castList > tei-head > tei-hi[rend=center]": {
          textAlign: "center"
        },
        "tei-sp": {
          display: "block",
          marginBottom: "1em",
        },
        "tei-sp[rend=center]": {
          textAlign: "center"
        },
        "tei-stage[type=business]": {
          display: "block"
        },
        "tei-stage[rend=center]": {
          textAlign: "center"
        },
        "tei-head": {
          display: "block",
        },
        "tei-body > tei-head": {
          fontSize: "180%",
          textIndent: "-0.5em",
        },
        "tei-floatingText": {
          display: "block"
        },
        "tei-front > tei-head": {
          fontSize: "180%",
          textIndent: "-0.5em",
        },
        "tei-front tei-byline": {
          fontSize: "130%",
          display: 'block'
        },
        "tei-div": {
          display: "block"
        },
        "tei-div[type=section] > tei-head": {
          textAlign: "center",
          fontStyle: "italic",
          fontSize: "130%",
        },
        "tei-div[type=notes] > tei-head": {
          textAlign: "center",
          fontStyle: "italic",
          fontSize: "130%",
        },
        "tei-lb:after": {
          content: "'\\a'",
          whiteSpace: "pre"
        },
        "tei-p": {
          display: "block",
          marginTop: "1em",
          marginBottom: "1em",
          textAlign: "justify"
        },
        "tei-q:before": {
          content: `"“"`
        },
        "tei-q:after": {
          content: `"”"`
        },
        "tei-lg": {
          display: "block",
        },
        "tei-l": {
          display: "block",
        },
        "tei-l[rend=indent1]": {
          marginLeft: "1em"
        },
        "tei-l[rend=indent2]": {
          marginLeft: "2em"
        },
        "tei-l[rend=indent3]": {
          marginLeft: "3em"
        },
        "tei-l[rend=indent4]": {
          marginLeft: "4em"
        },
          "tei-l[rend=indent5]": {
          marginLeft: "5em"
        }, 
        "tei-l[rend=indent6]": {
          marginLeft: "6em"
        },
        "tei-doctitle": {
          display: "block",
          fontSize: "150%"
        },
        "tei-div[type=act] > tei-head": {
          fontSize: "150%",
          marginTop: "3em",
          marginBottom: "1em"
        },
        "tei-div[type=scene] > tei-head": {
          fontSize: "120%",
          marginTop: "2em",
          marginBottom: "1em"
        },
        "tei-div[type=song]": {
          textAlign: "center"
        },        
        "tei-castGroup > tei-head": {
          fontSize: "100%",
          fontStyle: "italic",
          marginTop: "2em",
          marginBottom: "1em",
          marginLeft: "4em"
        },
        "tei-castGroup": {
          display: "block",
          marginBottom: "1em"
        },
        "tei-castList": {
          fontSize: "100%",
          marginTop: "1em",
          marginBottom: "1em"
        },
        "tei-div[type=notes]": {
          display: "block"
        },
        "tei-list > tei-item[rend=center]": {
          textAlign: "center" 
        },  
        "tei-list": {
          marginTop: "1em",
          marginBottom: "1em",
          display: "block"
        },
        "tei-list > tei-item": {
          display: "block"
        },
        "tei-castGroup > tei-castItem": {
          display: "block"
        },
        "tei-hi[rend=underline]": {
          textDecoration: "underline"
        },
        "tei-stage[rend=underline]": {
          textDecoration: "underline"
        },
        "tei-stage[rend=right]": {
          textIndent: "5em",
          display: "block"
        },
        "tei-del, .se_del": {
          textDecoration: "line-through"
        },
        "tei-figure > tei-figDesc": {
          fontStyle: "italic"
        },
        "tei-note": {
          display: "none",
        },
        "tei-titlePage": {
          textAlign: "center"
        }
      },
    },
  },
  palette: {
    primary: {
      main: mainColor,
    },
    secondary: {
      main: "#fbebda",
    },
    error: {
      main: red.A400,
    },
    background: {
      default: "#fff",
    },
  },
})

export default theme
