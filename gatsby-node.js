exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions
  await makeSynoptic(createPage, reporter, graphql)
  await makeIntroduction(createPage, reporter, graphql)
}

async function makeIntroduction(createPage, reporter, graphql) {
  const component = require.resolve(`./src/templates/introduction.tsx`)

  const result = await graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            html
            frontmatter {
              path
              title
            }
          }
        }
      }
    }
  `)
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }
  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: node.frontmatter.path,
      component,
      context: {
        html: node.html,
        title: node.frontmatter.title
      }
    })   
  })
}

async function makeSynoptic(createPage, reporter, graphql) {
  const component = require.resolve(`./src/gatsby-theme-ceteicean/components/Ceteicean.tsx`)

  const result = await graphql(`
    query {
      allCetei {
        nodes {
          prefixed
          elements
          parent {
            ... on File {
              name
            }
          }
        }
      }
    }
  `)
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  // Create combined pages

  const sections = [
    'front',
    'Act1_Scene1',
    'Act1_Scene2',
    'Act1_Scene3',
    'Act2_Scene1',
  ]

  sections.map((name) => {
    const path = name === 'Act2_Scene1' ? `synoptic-Act2_Scenes1-2` : `synoptic-${name}`
    const section = result.data.allCetei.nodes.reduce((acc, node) => {
      if (node.parent.name === `BC_${name}`) {
        // wrap the two TEI files in a single <TEI> element
        acc.prefixed = `<tei-TEI data-xmlns=\"http://www.tei-c.org/ns/1.0\" data-origname=\"TEI\" data-origatts=\" xmlns\">${node.prefixed}`
        acc.elements = node.elements
      } else if (node.parent.name === `BR_${name}` || (name === 'Act2_Scene1' && node.parent.name === `BR_Act2_Scenes1-2`)) {
        acc.prefixed += `${node.prefixed}</tei-TEI>`
        // merge element lists
        for (const el of node.elements) {
          if (acc.elements.indexOf(el) === -1) {
            acc.elements.push(el)
          }
        }
        acc.name = path
      } 
      return acc
    }, {elements: [], prefixed: ''})
  
    createPage({
      path,
      component,
      context: section
    })
  })
}
