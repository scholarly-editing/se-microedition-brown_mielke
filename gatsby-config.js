const basePath = process.env.BASEPATH
const title = "John Brougham’s Columbus Burlesque"
const {spanners, getNotes} = require('./scripts/transformers')

module.exports = {
  pathPrefix: basePath,
  siteMetadata: {
    issue: "Volume 39",
    doi: '10.55520/4K9J248Z',
    group_order: 1,
    title: title,
    htmlTitle: "John Brougham’s <em>Columbus</em> Burlesque",
    description: `A Scholarly Editing micro-edition. ${title}. Edited by Laura L. Mielke and Rachel L. Brown.`,
    authors: [
      {
        "first": "Laura",
        "middle": "L.",
        "last": "Mielke",
        "affiliations": [
          "University of Kansas"
        ],
        "orcid": "0000-0001-5042-8978"
      },
      {
        "first": "Rachel",
        "middle": "Linnea",
        "last": "Brown",
        "affiliations": [
          "Marian University-Indianapolis"
        ],
        "orcid": "0000-0001-5636-2762"
      }
    ],
    menuLinks: [
      {
        name: 'introduction',
        link: '/'
      },
      {
        name: 'filibustero',
        link: '/BC_front'
      },
      {
        name: 'reconstructed',
        link: '/BR_front'
      },
      {
        name: 'synoptic',
        link: '/synoptic-front'
      },
    ]
  },
  plugins: [
    `gatsby-plugin-material-ui`,
    {
      resolve: `gatsby-theme-ceteicean`,
      options: {
        applyBefore: [getNotes],
        applyAfter: [spanners],
        namespaces: {
          "http://www.tei-c.org/ns/1.0": "tei",
          "http://www.tei-c.org/ns/Examples": "teieg",
          "http://www.w3.org/2001/XInclude": "xi"
        }
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `static/tei`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/introduction`,
      },
    },
    `gatsby-transformer-remark`,
    `gatsby-transformer-json`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/data`,
      },
    },
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/images/`,
      },
    },
  ],
}
