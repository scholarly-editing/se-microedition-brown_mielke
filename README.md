# John Brougham’s _Columbus_ Burlesque
## Edited by Laura L. Mielke, University of Kansas and Rachel Linnea Brown, Marian University-Indianapolis
## Digital Edition code by Raffaele Viglianti, Laura L. Mielke, and Rachel Linnea Brown

### Scholarly Editing, Volume 39, 2022

This micro-edition is published on the [Scholarly Editing website](https://scholarlyediting.org/issues/39/john-broughams-columbus-burlesque).

The repository is **archived**. No further changes will be made to this repository. For a general micro-edition template please see [this GitLab repository](https://gitlab.com/scholarly-editing/se-microedition-template).
