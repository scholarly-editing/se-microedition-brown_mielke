<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../tei_se-brown_mielke.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<noteGrp xmlns="http://www.tei-c.org/ns/1.0" xml:id="BC_notes">
    <note type="theatrical" xml:id="note1">These characters, up to Columbia, are satirical
        depictions of real historical figures from the fifteenth century; <quote>“Arragon”</quote>
        is a misspelling of Aragon. Actors John Brougham, Mark Smith, and Lizzie Weston Davenport
        received top billing in December 1857 advertisements for <title>Columbus El
            Filibustero!!</title>. <ref
            target="https://www.npg.org.uk/collections/search/person/mp87471/lizzie-weston-nee-elizabeth-jackson"
            >Davenport</ref> was particularly popular.</note>
    <note type="definition" xml:id="note2">An employee whose position requires little effort but
        offers a social and/or financial benefit. <bibl><title>OED Online</title>, sinecure, n.,
            2</bibl>.</note>
    <note type="historical" xml:id="note3">Nunez de Balboa (1475-1519), Cortez (1485-1547), Vespucci
        (1451-1512), and de Leon (1460-1521) all sailed for Spain to survey, claim, and establish
        power over lands in the Americas.</note>
    <note type="historical" xml:id="note4">Alternative spelling for sepoy, or an Indian soldier
        serving under the British. <bibl><title>OED Online</title>, sepoy, n</bibl>. In May 1857,
        sepoys initiated the First War of Indian Independence, an uprising against the British East
        India Company that lasted for more than a year and was officially declared over in July
        1859.</note>
    <note type="musical" xml:id="note5">Likely a reference to Auber’s 1838 historical opera
            <title>Gustave III</title>, which depicts the assassination of Gustav III, King of
        Sweden, in 1792. Brougham sets new lyrics to popular 1800s songs throughout <title>Columbus
            El Filibustero!!</title>, ranging from operatic and minstrel to patriotic and folk
        genres. The original song or production name indicates the appropriate score.</note>
    <note type="definition" xml:id="note6">Misspelling of encomiastic, or full of praise.
                <bibl><title>OED Online</title>, encomiastic, adj. A</bibl>.</note>
    <note type="definition" xml:id="note7">A likely reference to shins or knees (i.e. a command to
        kneel). <bibl><title>OED Online</title>, marrowbone, n., 2a.</bibl></note>
    <note type="historical" xml:id="note8">Joseph Smith founded Mormonism in 1830 and was gunned
        down in Illinois in 1844. Julius Caesar was a Roman general, statesman, and eventually
        dictator who was assassinated in 44 BCE. Bringham Young succeeded Joseph Smith in power and
        led a group of Mormons to Utah in 1847, where he eventually became Utah Territory’s first
        governor. Nebuchudnezzar (also spelled Nebuchadnezzar) ruled Babylon from 605 BCE to 562 BCE
        and destroyed Jerusalem in 586 BCE. Brougham, in other words, juxtaposes contemporary and
        historical figures.</note>
    <note type="historical" xml:id="note9">Mary, Queen of Scots (1542–87), who ruled Scotland from
        1542 to 1567, was known for her beauty and talents. After her rule, she was held as a
        prisoner in England under Queen Elizabeth I and eventually was executed. </note>
    <note type="definition" xml:id="note10">An alternative spelling for Nereid, or a sea nymph.
                <bibl><title>OED Online</title>, nereid, n., A1a</bibl>.</note>
    <note type="definition" xml:id="note11">A drum. <bibl><title>OED Online</title>, tympanum, n.,
            1</bibl>.</note>
    <note type="definition" xml:id="note12">Name for the Muslim peoples of northwestern Africa.
                <bibl><title>OED Online</title>, Moor, n., 2</bibl>.</note>
    <note type="definition" xml:id="note13">A derogatory name for foolish or stupid people.
                <bibl><title>OED Online</title>, pudding-head, n</bibl>.</note>
    <note type="definition" xml:id="note14">A charlatan. <bibl><title>OED Online</title>,
            mountebank, n., 1b</bibl>.</note>
    <note type="definition" xml:id="note15">Something that marks equal parts, such as the equator,
        which divides the northern and southern hemispheres. <bibl><title>OED Online</title>,
            equinoctial, adj., 3a</bibl>. His allusion also continues the ongoing pun about whether
        the world’s southern hemisphere corresponds (i.e. sends letters) with the northern
        hemisphere.</note>
    <note type="definition" xml:id="note16">An alternative spelling for the now-obsolete word
        caitiff, which is a miserable person, especially a captive. <bibl><title>OED Online</title>,
            caitiff, n., A 1 and 2</bibl>.</note>
    <note type="definition" xml:id="note17">Columbus means dove in Latin. While the historical
        figure we call Columbus went by many names, including Cristobal Colon in Spain, the Latinate
        surname seems to have been of his own choosing.</note>
    <note type="musical" xml:id="note18">A popular German opera by Carl Maria von Weber
        (1786-1826).</note>
    <note type="musical" xml:id="note19"><title>Il Trovatore</title> was a melodramatic Italian
        opera by Giuseppe Verdi that premiered in 1853. While invoking a real contemporary tune,
        Brougham writes <quote>“Ecco Italiani,”</quote> or faux Italian, lyrics. In the ensuing
        exchange, the chorus first mocks Columbus, threatening to tie him up (<quote>“Ti
            himup”</quote>), and then spews nonsensical Italianesque phrases. Columbus responds to
        the <quote>“crowdo”</quote> by exclaiming <quote>“Bi guingo”</quote> (or “By jingo”) and
        lamenting that he is miserable. After the chorus promises to detain him, Columbus curses
            (<quote>“damit”</quote>).</note>
    <note type="definition" xml:id="note20">Misspelling of cosmogony, or an account of the
        universe’s beginning. <bibl><title>OED Online</title>, cosmogony, n. 2b</bibl>.</note>
    <note type="definition" xml:id="note21">Formerly, particularly when referring to a person’s past
        rank. <bibl><title>OED Online</title>, ci-devant, n., 1 and 2</bibl>.</note>
    <note type="musical" xml:id="note22">Brougham’s coinage, this is a biographical song performed
        in a style between singing and speaking (known as <emph>recitative</emph> delivery) and
        followed by two verses sung to a <quote>“familiar tune.”</quote>
        <bibl><title>OED Online</title>, recitative, n. A1</bibl>. In standard edited English,
        Columbus’s faux Italian <title>Introductory Recitative</title> reads: “My simple story that
        you request / Oh give an ear to my relation, / But if this foreign lingo, you cannot digest,
        / I’ll try the purist Anglo Saxon.” Columbus/Brougham subsequently drops the faux Italian
        accent for the two verses of the <title>Aria Familiaria</title>.</note>
    <note type="definition" xml:id="note23">An expression of surprise. <bibl><title>OED
                Online</title>, jingo, int. and n.1</bibl>.</note>
    <note type="historical" xml:id="note24">Presumably King Louis XI, who ruled France from 1461 to
        1483. He was succeeded by son Charles VIII, who ruled until 1498. The British King refers to
        Henry VII. </note>
    <note type="definition" xml:id="note25">Surrounding. <bibl><title>OED Online</title>,
            circumambient, adj. 1a</bibl>.</note>
    <note type="definition" xml:id="note26">“A cat in the meal-tub” was an American colloquialism
        meaning <quote>“a hidden danger; a cause for suspicion, a concealed motive.”</quote>
        <bibl><title>OED Online</title>, cat, n.1</bibl>.</note>
    <note type="historical" xml:id="note27">Joint stock refers to <quote>“Stock or capital
            contributed and owned by a number of persons jointly”</quote> (<bibl><title>OED
                Online</title>, joint stock, n.</bibl>), and a bear market is one marked by
        prolonged decline. Brougham refers here to the <ref
            target="http://www.americaslibrary.gov/jb/reform/jb_reform_goldlost_2.html">Panic of
            1857</ref>, which started August 24, 1857, after the <name>SS Central America</name>, a
        ship carrying California gold for already beleaguered US banks, sank in a hurricane. Without
        gold to meet the demand of anxious customers, banks began to fail. A three-year depression
        with international repercussions followed.</note>
    <note type="definition" xml:id="note28">Far enough (Latin).</note>
    <note type="definition" xml:id="note31">Temporary. Abreviation for pro tempore (Latin).
                <bibl><title>OED Online</title>, pro tem, adj</bibl>.</note>
    <note type="historical" xml:id="note29">The Erie Canal, which connected the Hudson River to Lake
        Erie, opened in 1825 and secured New York City’s dominance as an eastern port and fueled
        economic growth across the state. </note>
    <note type="historical" xml:id="note30">In the mid-nineteenth century, <hi rend="italics"
            >filibuster</hi> was the name given to a private US citizen who undertook a military
        expedition into a sovereign nation in pursuit of political power (Mexico, Nicaragua, Cuba,
        and Honduras were all targets). Filibusters often sought to bolster the institution of
        slavery.</note>
    <note type="historical" xml:id="note32">Ironic pun on the famous lines by Oliver Goldsmith
            (1730-74)--<quote>“Man wants but little here below, / Nor wants that little
            long”</quote>--and the name of a well-known nineteenth-century Wall Street speculator,
        Jacob Little (1794-1865).</note>
    <note type="theatrical" xml:id="note33">Columbus’s lines in this portion of the play are largely
        identical with "The Vision of Columbus," an earnest poem Brougham delivered at a benefit for
        the Mount Vernon Association held at New York’s Academy of Music on December 18, 1858. In
        the poem, Columbus envisions the continent’s sublime landscape and its agricultural bounty,
        especially the development of two regional crops: wheat (<quote>“life-giving grain”</quote>)
        on the Western plains and cotton (<quote>“small shrub”</quote>) in the South. For the play,
        Brougham adds a humorous reference to tobacco (<quote>“withered weed”</quote>). ”The Vision
        of Columbus” appears in the <bibl>1881 volume <title>Life, Stories, and Poems of John Brougham</title>, edited by William Winter (Boston: J. R. Osgood), pp.
        458-60</bibl>.</note>
    <note type="definition" xml:id="note34">Fleeting. <bibl><title>OED Online</title>, fugacious,
            adj., 1a</bibl>.</note>
    <note type="definition" xml:id="note35">Alternative spelling for Pastolus, a river in Turkey (now
        named Sart Çayı) once famed for its gold and associated with the legend of King
        Midas.</note>
    <note type="historical" xml:id="note36">Columbus catalogues recent US social and technological
        advancements, including burgeoning towns, railroad lines (<quote>“iron ways”</quote>), and
        telegraph networks (<quote>“electric nerves”</quote>).</note>
    <note type="historical" xml:id="note37">The Dead Rabbits were an antebellum New York gang
        affiliated with Democratic Tammany Hall who clashed with nativist groups like the
        Know-Nothing party and the Bowery Boys. July 4-5, 1857, a riot occurred between the Dead
        Rabbits and the Bowery Boys, who were anti-Irish and anti-Catholic. <bibl><title>The Gangs
                of New York</title> (2002, directed by Martin Scorsese)</bibl> depicts these as well
        as subsequent conflicts.</note>
    <note type="historical" xml:id="note38">An order of knighthood first established in 1430,
        associated with Spain and Roman Catholic ideals.</note>
    <note type="definition" xml:id="note39">Intelligence. <bibl><title>OED Online</title>, nous, n.,
            1</bibl>.</note>
    <note type="definition" xml:id="note40">A building at a port of entry where fees or duties for
        incoming goods are assessed.</note>
    <note type="historical" xml:id="note41">Political organizer who founded the Empire Club. To
        support the Tammany Hall political machine, Rynders and the Empire Club used voter
        intimidation to help swing elections for Democratic, anti-abolitionist candidates, including
        US Presidents James K. Polk, Franklin Pierce, and James Buchanan, Jr. As the fifteenth US
        President, Buchanan (1857-1861) appointed Rynders as a US Marshal.</note>
    <note type="definition" xml:id="note42">Invented in 1786, threshing machines separate seeds from
        stalks and husks. Such equipment significantly sped up harvests and processing and are still
        used today (albeit technologically updated).</note>
    <note type="musical" xml:id="note43">A possible reference to Michael William Balfe’s <title>The
            Enchantress</title>, a three-act opera that opened in 1845 at the Theatre Royal, Drury
        Lane, New York. The opera includes the patriotic tune <ref
            target="http://hdl.loc.gov/loc.music/sm1849.481240">“Raise the Bright Flag of Columbia.”</ref>
    </note>
    <note type="definition" xml:id="note44">An alcoholic drink taken to help the imbiber fall
                asleep.<bibl><title>OED Online</title>, night-cap, n., 4</bibl>.</note>
    <note type="musical" xml:id="note45">Faux Italian for a cordial (i.e. friendly) duet.</note>
    <note type="definition" xml:id="note46">Daily newspapers. <bibl><title>OED Online</title>,
            diurnals, n. B3</bibl>.</note>
    <note type="definition" xml:id="note47">Slang for cunning. <bibl><title>OED Online</title>,
            downey, n. 1</bibl>.</note>
    <note type="definition" xml:id="note48">A poetic spelling of Sovereign, which means to have
        power over others. <bibl><title>OED Online</title>, sovran, adj</bibl>.</note>
    <note type="definition" xml:id="note49">A dressing gown.<bibl><title>OED Online</title>, robe de
            chambre, n</bibl>.</note>
    <note type="definition" xml:id="note50">Wearing a cap to bed. <bibl><title>OED Online</title>,
            nightcap, n. 1a</bibl>.</note>
    <note type="historical" xml:id="note51"><bibl>William H. Prescott published <title>The History
                and Reign of Ferdinand and Isabella the Catholic</title> (1837)</bibl>, and
            <bibl>Washington Irving published <title>A History of the Life and Voyages of
                Christopher Columbus</title> (1828)</bibl>.</note>
    <note type="historical" xml:id="note52">Samuel French, publisher of dramas; this pun follows a
        common complaint that French culture had an outsized influence in contemporary
        theater.</note>
    <note type="historical" xml:id="note53">Thomas Crawford produced in 1856 multiple models for the
            <ref target="https://www.aoc.gov/capitol-hill/other-statues/statue-freedom">Statue of
            Freedom</ref> that would be placed on top of the US Capitol Dome.</note>
    <note type="historical" xml:id="note54">Greek orator <quote>“who roused Athens to oppose Philip
            of Macedon and, later, his son Alexander the Great”</quote>
        <bibl>(Murphy, James J. Murphy, <ref
                target="https://www.britannica.com/biography/Demosthenes-Greek-statesman-and-orator"
                >"Demosthenes,"</ref> Encyclopedia Britannica).</bibl> To overcome a speech
        impediment, Demosthenes allegedly filled his mouth with pebbles and then talked.</note>
    <note type="historical" xml:id="note55">The Panic of 1857, which started in the US, also
        impacted Britain.</note>
    <note type="definition" xml:id="note56">Slang for "enough said."</note>
    <note type="definition" xml:id="note58">Cold-without (Latin), though this phrase also appears in
        nineteenth-century texts to reference cold drinks (i.e. in contrast to hot spirits).</note>
    <note type="musical" xml:id="note57">Though a popular song in the mid-1800s--Francis Scott Key
        penned the lyrics in 1814 and set them to the English drinking song “To Anacreon in
        Heaven”--"The Star-Spangled Banner” was not adopted as the US national anthem until 1931.
        Brougham’s patriotic reimagining lifts several phrases from Key, including <quote>“O’er the
            land of the free and the home of the brave.”</quote> Doing so reveals Brougham’s
        satirical limits; while he is critical of nineteenth-century political corruption in
            <title>Columbus El Filibustero!!</title>, Brougham ultimately adheres to patriotic
        diction.</note>
    <note type="definition" xml:id="note59">An ideal land.</note>
    <note type="historical" xml:id="note60">Punning reference to the First War of Indian
        Independence.</note>
    <note type="definition" xml:id="note61">An informal phrase meaning to have control over
        something or someone. <bibl><title>OED Online</title>, lock, n. 2, P12</bibl>.</note>
    <note type="definition" xml:id="note62">Idle chatter.<bibl><title>OED Online</title>, palaver,
            n., 2a and 2b</bibl>.</note>
    <note type="historical" xml:id="note63">This scene adapts the story from Columbus’s first voyage
        that the crew found evidence of nearby land in the form of birdsong, scents in the air,
        freshwater weeds and tree branches, and a carved staff. </note>
    <note type="definition" xml:id="note64">A fruit or vegetable pie.</note>
    <note type="theatrical" xml:id="note65">William E. Burton (1802-60) was a British comic actor,
        manager, and editor who, like Brougham, made his theatrical and editorial careers in the
        US In New York from the late 1840s through the 1850s, he managed Burton’s Theatre and
        subsequently Burton’s New Theatre. <title>Columbus El Filibustero!!</title> debuted at
        Burton’s on December 30, 1857. Brougham and Burton were known for witty exchanges onstage
        and off. Once, Brougham and a companion were joined by Burton at a cafe, and when that
        companion asked Burton if he ever read <title>Diogenes, Hys Lantern</title>, the comic
        journal Brougham helped found in 1852, Burton declared, <quote>“'Never, except when I am
            drunk!'”</quote> Brougham quickly responded, <quote>“Then thank God, we are always sure
            of one faithful reader!”</quote>
        <bibl>(Frank Luther Mott, A History of American Magazines, vol. 2 of 5, 1938, p.
        182)</bibl>.</note>
    <note type="theatrical" xml:id="note66">Characters played by William E. Burton, owner and
        namesake of Burton’s Theatre. They include Aminadab Sleek from <title>The Serious
            Family</title> (1849) and Timothy Toodles from <title>The Toodles</title> (1853).</note>
    <note type="definition" xml:id="note67">A colloquial name for American thrushes, a bird whose
        cry resembles the “mewing” of a cat.<bibl><title>OED Online</title>, catbird, n., 1a and
            1b</bibl>.</note>
    <note type="historical" xml:id="note68">The Adriatic was a steamship for the Collins line. On
            <bibl>April 8, 1856, the <title>New York Herald</title></bibl> proclaimed that the
        Adriatic was <quote>“not only the largest vessel built in this country, but that she is
            designed to be the fastest ocean steamer in the world”</quote> (1). Great fanfare
        surrounded the Adriatic’s 1856 launch, including an audience of at least 60,000 observers.
        The launch was ill-fated, however, since the Adriatic’s anchor failed to hold it. The great
        ship crashed into a pier when launched, though only suffered light scratches. She did not
        sail for Collins again until 1857 and was sold off in bankruptcy proceedings in 1858. The
        Dodworth Band, formed by Allen and Harvey Dodworth, was a popular mid-1800s New York City
        brass band known to play on board ships. In 1985, a living history group resurrected the
            <ref target="https://dodworth.org/about-the-band/">Dodworth Band</ref>, and they still
        feature period-appropriate scores, costumes, and instruments.</note>
    <note type="definition" xml:id="note69">American colloquialism meaning “I declare.”
                <bibl><title>OED Online</title>, swow, v., 2</bibl>.</note>
    <note type="definition" xml:id="note70">A legendary ghost ship whose narrative origins are
        associated with the Dutch East India Company.</note>
    <note type="historical" xml:id="note71">In Brougham’s day, Coney Island in the borough of
        Brooklyn, New York, was dotted with seaside resorts--a contrast to the amusement park
        attractions offered on what is today a peninsula.</note>
    <note type="historical" xml:id="note72">Castle Garden, known today as the Castle Clinton
        National Monument, began as the South West Battery, a heavily armed fort on the Battery that
        was completed in 1811 to protect New York City. In 1823, the city took possession of the
        fort and soon it became an entertainment hub (with a restaurant, opera house, and theater)
        renamed Castle Garden. According to the <bibl><ref
                target="https://www.nps.gov/cacl/learn/historyculture/index.htm">National Park
                Service</ref></bibl>, <quote>“Many new inventions were demonstrated there, including
            the telegraph, Colt revolving rifles, steam-powered fire engines, and underwater
            electronic explosives.”</quote> From 1855 to 1890, the Castle Garden served as an
        official US immigration center.</note>
    <note type="historical" xml:id="note73">A cannon on Governors Island, south from Castle Garden
        on the Battery, was fired on festive occasions. Consider a <bibl>June 17, 1860, account in
            the <title>New York Herald</title></bibl> of the city’s reception of a Japanese
        delegation at the Battery: <quote>“The Castle Garden, projecting a short distance from the
            Battery, was literally covered with spectators…. [The approach of the ship] was
            announced by the booming of the cannon from Governor’s Island, again and again, bands of
            music at Castle Garden and on the pier joining Dodworth’s Band stationed on the Alida in
            playing ‘Hail Columbia.’ When the steamer reached Castle Garden salutes were fired from
            the Battery and a government steamer, and the booming of cannon continued until the
            steamer was moored to the dock.”</quote> The arrival is illustrated in the <bibl>June
            30, 1860, issue of <title>Frank Leslie’s Illustrated Newspaper</title></bibl>.</note>
    <note type="definition" xml:id="note74">A term of endearment with US Naval origins. According to
        the <bibl><ref
                target="https://www.defense.gov/Explore/News/Article/Article/604121/face-of-defense-sailor-receives-old-tar-award/"
                >US Department of Defense</ref></bibl>, US sailors preparing for battle
            <quote>“would dip the knot of their long hair in tar, which would then harden and
            protect their necks from blows from behind. Some sailors soon became known as ‘tars.’ An
            ‘Old Tar’ was one who was honored and respected for his knowledge, wisdom and long
            experience at sea.”</quote></note>
    <note type="definition" xml:id="note75">A slang word for the act of engaging in political
        corruption, particularly by bringing extra voters into a district to swing an
                election.<bibl><title>OED Online</title>, pipe lay, v. 1</bibl>.</note>
    <note type="historical" xml:id="note76">A likely reference to Dan Kerrigan, a fighter who owned
        a bar on Cherry Street and was affiliated with the Dead Rabbits. </note>
    <note type="musical" xml:id="note78">A melodramatic opera by Giacomo Meyerbeer that opened in
        Paris in 1831.</note>
    <note type="definition" xml:id="note79">Although a gripsack can be defined as a traveler’s bag,
        here, such nonsensical words are intended to create the “Infernal Row” and “Confusion” of
        the scene’s end. <bibl><title>OED Online</title>, gripsack, n</bibl>.</note>
    <note type="definition" xml:id="note80"><quote>“Noisy disorder.”</quote>
        <bibl><title>OED Online</title>, Babelish, adj. 2</bibl>. In the book of Genesis, Babylon or
        Babel is the city whose ambitious residents attempt to build a tower all the way to the
        heavens. God curbed their potential by causing humanity to speak in multiple
        languages.</note>
    <note type="definition" xml:id="note81">Morose or despondent. <bibl><title>OED Online</title>,
            dyspeptic, adj., 2b</bibl>.</note>
    <note type="definition" xml:id="note82">An alternative spelling of ennuied, meaning bored.
                <bibl><title>OED Online</title>, ennui, v</bibl>.</note>
    <note type="theatrical" xml:id="note83">Ballet move in which dancers cross their feet during a
        vertical leap. <bibl><title>OED Online</title>, entrechat, n</bibl>. In 1850s New York, an
        evening’s theatrical line-up might include a ballet performance.</note>
    <note type="historical" xml:id="note84">A reference to the 1857 political wrangling in New York
        City between the Metropolitan police force (supported by the Bowery Boys and appointed by
        the New York state legislature in response to corruption) and Municipal police force
        (supported by the Dead Rabbits and appointed by Mayor Fernando Wood). The Municipal police
        force and Wood initially refused to cede control to the newly appointed Metropolitan police
        force, leading to confusion about who had enforcement authority.</note>
    <note type="historical" xml:id="note85">As part of the Spanish Inquisition under King Ferdinand
        and Queen Isabella, many Jews, including those who had converted to Christianity, were
        burned at the stake.</note>
    <note type="historical" xml:id="note86">First demonstrated in the US in 1844, and quickly
        developed for commercial use, the telegraph represented very modern communication
        technology.</note>
    <note type="historical" xml:id="note87">Located on the southern tip of the island of
        Newfoundland, Canada, Cape Race played an important role in the New York Associated Press of
        the mid-1800s. As detailed in a brief <bibl><ref
                target="https://timesmachine.nytimes.com/timesmachine/1857/11/06/78510325.pdf"
                    ><title>New York Times</title> article</ref> of November 6, 1857</bibl>, a yacht
        of reporters stationed eight miles below Cape Race met transatlantic ships for the latest
        European news, which they then promptly telegraphed to the United States and Canada. </note>
    <note type="historical" xml:id="note88">The <bibl><ref
                target="https://www.ap.org/about/our-story/">Associated Press</ref></bibl> began
        when five New York City newspapers created a pony express route from Alabama in 1846 to more
        quickly and economically report on the Mexican War.</note>
    <note type="definition" xml:id="note89"><quote>“small wares or useful articles made in New
            England or the northern States,”</quote>
        <bibl><title>OED Online</title>, n. and adj.</bibl>. Here it is used punningly to also
        signal northern ideas.</note>
    <note type="historical" xml:id="note90">Isabella has large bills to pay at luxury goods and
        clothing stores headquartered in New York City. According to <bibl><ref
                target="https://www.sothebys.com/en/articles/tiffany-200-year-history">Vivienne
                Becker</ref></bibl>, Tiffany &amp; Co. (misspelled here as Tiffanny) was founded in
        1838 as “Tiffany &amp; Young,” and by 1853, founder Charles Lewis Tiffany gave the business
        its current name: Tiffany &amp; Co. Tiffany's is still internationally known for its jewelry
        and luxury household wares. J. Beck &amp; Co. was a contemporary discount clothing store on
        Broadway. In 1857, Beck's advertised "Shawls! Cheap from auction," "Broche, 60 per cent
        below cost of importation," and the "cheapest Silks ever offered" in the <title>New York
            Daily Herald</title>. Unlike Tiffany’s, Beck's is not well known or in operation
        today.</note>
    <note type="musical" xml:id="note91">An Italian opera composed in 1851 by Giuseppe Verdi and
        based on a Victor Hugo play.</note>
    <note type="musical" xml:id="note92">A possible reference to a 1783 British comedic opera of the
        same name about Irish soldiers during the American revolution, an 1828 musical farce of the
        same name, or the 1855 tune published in Boston, “A Poor Soldier Boy.”</note>
    <note type="musical" xml:id="note93">A traditional jig with many versions, though likely
        Scottish or Irish in origin. Also known as “Merrily Kiss The Quaker’s Wife.”</note>
    <note type="musical" xml:id="note94">A popular British song written by Charles William Glover in
        1847 about French lovers.</note>
    <note type="musical" xml:id="note95">Song popularized by antebellum blackface minstrel
        performers like the Ethiopian Serenaders. As <bibl>William J. Mahar traces in <title>Behind
                the Burnt Cork Mask</title> (1999)</bibl>, many variants of the song circulated, and
        most focused on <quote>“a female beloved who is sold by a wicked master”</quote>
        (297).</note>
    <note type="historical" xml:id="note96">Phineas Taylor Barnum (1810-91), huckster-showman and
        operator of New York’s American Museum, showcased such humbug spectacles as John C.
        Fremont’s wooly horse (it was never Fremont’s) and the Fiji Mermaid (a compilation of monkey
        and fish remains). He also made spectacles of human beings, including the Nurse of
        Washington (enslaved woman Joice Heth) and General Tom Thumb (Charles Stratton).</note>
    <note type="theatrical" xml:id="note97">White actors in blackface. “Ethiopian” was a term for a
        person of African descent in the eighteenth century, but in the 1840s came to refer to the
        popular entertainment form of minstrelsy. <bibl><title>OED Online</title>, Ethiopian, n. 1a,
            1d</bibl>.</note>
    <note type="theatrical" xml:id="note98">Character in Brougham’s adaptation of Charles Dickens’s
        1848 novel <title>Dombey and Son</title> that was performed by <ref
            target="https://digital.library.illinois.edu/items/8da65060-4e7d-0134-1db1-0050569601ca-5"
            >William E. Burton</ref> when it was staged at Burton’s Theatre.</note>
    <note type="theatrical" xml:id="note99">Historical figures featured in <bibl>John Brougham's
            burlesque of the seventeenth-century Jamestown colony, <title>Po-Ca-Hon-Tas, or the
                Gentle Savage</title> (1855)</bibl>. Brougham performed the role of Powhatan, and
        the <foreign>dramatis personae</foreign> for the play describes John Smith as having
            <quote>“a crew of Fillibusters.”</quote></note>
    <note type="definition" xml:id="note100">Antiquated spelling of masque, a highly symbolic form
        of court drama originating in the seventeenth century and featuring intricate costuming,
        music, props, and music. <bibl><title>OED Online</title>, masque, n., I1a</bibl>.</note>
    <note type="historical" xml:id="note101">In the aftermath of the 1854 passage of the Kansas-Nebraska Act, which allowed the two territories to self-determine whether they would permit slavery, conflict erupted in Kansas Territory and western Missouri. This period,
        1854-57, was collectively known as “Bleeding Kansas.” To promote the cause of making Kansas
        a free state, white settlers from Massachusetts founded the town of Lawrence in 1854.
        Missouri “bushwhackers” subsequently sacked Lawrence on May 21, 1856. Lawrence saw further
        violence during the Civil War: Quantrill’s Raid of August 21, 1863, resulted in the death of
        150 men and boys. </note>
    <note type="definition" xml:id="note102">A pun on constitution (i.e. physical and/or mental
        ability) and the US Constitution. <bibl><title>OED Online</title>, constitution, n.,
            5a</bibl>.</note>
    <note type="historical" xml:id="note103">In 1857, General William S. Harney planned to lead a US
        military force into Utah Territory, where the Mormons had settled after being forced out of
        Missouri. The troops didn’t arrive in Utah until 1858, and by then, General Harney was no
        longer in command--he had stayed behind to help mediate “Bleeding Kansas.” While no
        significant confrontations between Mormon settlers and the US military occurred, Mormon
        militia members did dress as Paiutes and kill 120 members of a wagon train in September 1857
        in what came to be known as the Mountain Meadows Massacre.</note>
    <note type="historical" xml:id="note104">A possible reference to Spanish-American tensions of
        this time over Cuba and the US filibustering campaign there in 1850 to try and annex
        it.</note>
    <note type="historical" xml:id="note105">Brougham adapts for the closing spectacle the famous
        Columbian “anecdote of the egg,” an apocryphal sixteenth-century story detailed by
            <bibl>Washington Irving in <title>A History of the Life and Voyages of Christopher
                Columbus</title>, Volume I (1828), p. 275</bibl>. According to Irving’s source,
        Benzoni, some Spanish courtiers dismissed Columbus’s 1492 voyage and discovery of
        present-day North America as a simple feat anyone could have accomplished. To show the
        courtiers that anything seems simple once someone else tells you how to do it, Columbus
        allegedly challenged the courtiers to stand an egg on end. When they failed, Columbus
        smashed an egg on one side so it could freely stand.</note>
    <note type="theatrical" xml:id="note106">“Magic lantern” or “phantasmagoria” technology (likely
        what the <title>Columbus Reconstructed</title> promptbook refers to as a <quote>“Chinese
            lantern”</quote>) involved the use of multiple painted slides mounted on a mobile light
        projector to create static, moving, growing, or shrinking images (<bibl>Terence Rees,
                <title>Theatre Lighting in the Age of Gas</title>, London: The Society of Theatre
            Research, pp. 81-84</bibl>). What the stage directions describe here is the projection
        onto a screen at the back of the stage a magnified egg image that then was slowly
        transitioned into the Temple of Fame, with its arrangement of prominent Americans.</note>
    <note type="musical" xml:id="note107">According to the <bibl><ref
                target="https://www.loc.gov/item/ihas.200000008/">Library of Congress</ref></bibl>,
        “Hail Columbia” was originally an instrumental score known as “Washington’s March” that was
        performed at the first president’s 1789 inauguration. After is was given lyrics, "Hail Columbia" served as the unofficial US
        national anthem until 1931, when “The Star-Spangled Banner” officially replaced it; "Hail Columbia"
        presently serves as the US vice president’s official anthem. While other songs in
            <title>Columbus El Filibustero!!</title> are parodic or satirical, Brougham adopts much
        of the original patriotic tone, lyrics, and rhyme scheme of “Hail Columbia,” particularly
        its chorus: <quote>“Firm, united let us be, / Rallying round our liberty, / As a band of
            brothers joined, / Peace and safety we shall find.”</quote> Ending with
        “Hail Columbia,” Brougham turns from satire to patriotic spectacle.</note>

</noteGrp>
