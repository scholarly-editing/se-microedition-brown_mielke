const {JSDOM} = require("jsdom")
const serialize = require("w3c-xmlserializer")
const {readFileSync} = require("fs")

const TEINS = "http://www.tei-c.org/ns/1.0"

/* 
obj = {
  original: string
  prefixed: string,
  elements: string[]
}
*/
const spanners = (obj) => {  
  const newObj = Object.assign({}, obj)
  const {prefixed, elements} = newObj

  if (elements.indexOf('tei-addSpan') > -1 || elements.indexOf('tei-delSpan') > -1) {

    const addStack = []
    const delStack = []
    const toWrap = []

    const jdom = new JSDOM(prefixed, {contentType: 'text/xml'})
    const doc = jdom.window.document

    const walker = doc.createTreeWalker(doc.documentElement)

    while (walker.nextNode()) {
      const node = walker.currentNode
      if (node.nodeType === 1) {
        if (node.tagName.toLowerCase() === 'tei-addspan') {
          const spanTo = node.getAttribute('spanTo')

          if (spanTo) {
            addStack.push(spanTo.replace('#', ''))
          }
        } else if (node.tagName.toLowerCase() === 'tei-delspan') {
          const spanTo = node.getAttribute('spanTo')

          if (spanTo) {
            delStack.push(spanTo.replace('#', ''))
          }
        } else if (node.tagName.toLowerCase() === 'tei-anchor') {
          const id = node.getAttribute('id')

          const delStackLoc = delStack.indexOf(id)
          if (delStackLoc > -1) {
            delStack.splice(delStackLoc, 1)
          }

          const addStackLoc = addStack.indexOf(id)
          if (addStackLoc > -1) {
            addStack.splice(addStackLoc, 1)
          }
        }
      } else if (node.nodeType === 3) {
        let data = null

        if (delStack.length > 0) {
          if (node.textContent.replace(/\s+/, '') !== '') {
            data = {
              node,
              cl: ['se_del']
            }
          }
        }

        if (addStack.length > 0) {
          if (node.textContent.replace(/\s+/, '') !== '') {
            if (data) {
              data.cl.push('se_add')
            } else {
              data = {
                node,
                cl: ['se_add']
              }
            }
          }
        }

        if (data) {
          toWrap.push(data)
        }
      }
    }

    for (const data of toWrap) {
      const wrapper = doc.createElement('span')
      for (const c of data.cl) {
        wrapper.classList.add(c)
      }
      wrapper.textContent = data.node.textContent
      data.node.parentElement.replaceChild(wrapper, data.node)
    }

    newObj.prefixed = serialize(doc)
  }

  return newObj
}

const getNotes = (obj) => {
  // grab targeted notes from general notes file and place them in the current file.
  const BC = 'BC_'
  const BR = 'BR_'
  const isBC = obj.original.includes(BC)
  const isBR = obj.original.includes(BR)
  if (isBC || isBR) {
    const prefix = isBC ? BC : BR
    const newObj = Object.assign({}, obj)
    const {original} = newObj

    const jdom = new JSDOM(original, {contentType: 'text/xml'})
    const doc = jdom.window.document

    const notes = readFileSync(`static/tei/${prefix}notes.xml`, 'utf-8')

    const njdom = new JSDOM(notes, {contentType: 'text/xml'})
    const ndoc = njdom.window.document

    const noteGrp = doc.createElementNS(TEINS, 'noteGrp')

    doc.querySelectorAll('ptr').forEach(p => {
      const target = p.getAttribute('target').replace('#', '')
      const note = Array.from(ndoc.querySelectorAll('note')).filter(n => n.getAttribute('xml:id') === target)[0]
      noteGrp.append(note)
    })

    doc.documentElement.append(noteGrp)

    newObj.original = serialize(doc)

    return newObj
  }
  return obj
}


exports.spanners = spanners
exports.getNotes = getNotes
